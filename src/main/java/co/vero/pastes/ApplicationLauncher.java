package co.vero.pastes;

import co.vero.pastes.conf.SpringConfig;
import co.vero.pastes.utils.IOUtils;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Vitaly Romashkin on 22.04.2017.
 */
@Slf4j
public class ApplicationLauncher  {

    public static void main(String[] args) {
        startApplication(args);
    }

    private static void startApplication(String[] args)  {

        log.info("loading spring context...");
        ApplicationContext springContext = new AnnotationConfigApplicationContext(SpringConfig.class);

        log.info("creating vertx...");
        Vertx vertx = Vertx.vertx();

        log.info("deploying application verticle...");
        ApplicationVerticle verticle = new ApplicationVerticle(springContext);
        DeploymentOptions deploymentOptions = new DeploymentOptions().setConfig(loadConfiguration(args));
        vertx.deployVerticle(verticle,deploymentOptions);

    }

    private static JsonObject loadConfiguration(String[] args){

        if (args.length >= 2 && args[0].equalsIgnoreCase("-conf")){
            try {
                return IOUtils.loadConfiguration(args[1]);
            } catch (IOException e) {
                log.warn("could not load external configuration {}:{}",args[1],e.getMessage());
            }
        }

        try {
            return IOUtils.loadConfiguration(ApplicationLauncher.class.getResourceAsStream("../../../conf.json"));
        } catch (NullPointerException | IOException e) {
            log.warn("could not load conf.json inside a jar file:{}",e.getMessage());
        }

        JsonObject defaultConfiguration = new JsonObject();
        defaultConfiguration.put("host","localhost");
        defaultConfiguration.put("port",8080);
        defaultConfiguration.put("src-package","co.vero.pastes");
        defaultConfiguration.put("controller-packages",new JsonArray(Arrays.asList(new String[]{"co.vero.pastes.api.controllers"})));

        return defaultConfiguration;
    }

}
