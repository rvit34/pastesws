package co.vero.pastes.services;

import co.vero.pastes.domain.PasteEntry;
import co.vero.pastes.persistent.PersistentException;
import lombok.NonNull;

import java.util.List;
import java.util.Optional;

/**
 * Created by Vitaly Romashkin on 22.04.2017.
 */
public interface PastService {

    Optional<PasteEntry> addEntry(@NonNull PasteEntry entry) throws PersistentException;

    void updateEntry(String uuid, @NonNull String secretCode, @NonNull PasteEntry entry) throws PersistentException;

    boolean removeEntry(String uuid, @NonNull String secretCode) throws PersistentException;

    Optional<PasteEntry> getEntry(String uuid);

    List<PasteEntry> getPaginatedPublicEntries(int pageNumber, int limit);

    List<PasteEntry> getPaginatedPublicAndNotExpiredEntries(int pageNumber, int limit);
}
