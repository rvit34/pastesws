package co.vero.pastes.services;

/**
 * Created by Vitaly Romashkin on 22.04.2017.
 */
public class PastException extends Exception {
    public PastException() {
    }

    public PastException(String message) {
        super(message);
    }

    public PastException(String message, Throwable cause) {
        super(message, cause);
    }

    public PastException(Throwable cause) {
        super(cause);
    }

    public PastException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
