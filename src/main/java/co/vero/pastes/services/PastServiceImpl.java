package co.vero.pastes.services;

import co.vero.pastes.domain.PasteEntry;
import co.vero.pastes.persistent.PastEntryRepository;
import co.vero.pastes.persistent.PersistentException;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

import static co.vero.pastes.utils.GenUtils.generateSecretCode;

/**
 * Created by Vitaly Romashkin on 22.04.2017.
 */
@Slf4j
@Service("PastServiceImpl")
public class PastServiceImpl implements PastService {

    @Setter
    @Inject
    private PastEntryRepository repository;

    @Override
    public Optional<PasteEntry> addEntry(@NonNull PasteEntry entry) throws PersistentException {
        entry.setSecretCode(generateSecretCode(64));
        entry.setCreatedOnTimestamp(System.currentTimeMillis());
        return repository.addEntry(entry);
    }

    @Override
    public void updateEntry(String uuid, @NonNull String secretCode, PasteEntry pasteEntry) throws PersistentException{

        if (!repository.exist(uuid)){
            String warnMsg = String.format("could not update entry. Entry with uuid=%s is not found", uuid);
            log.warn(warnMsg);
            throw new PersistentException(warnMsg);
        }
        Optional<PasteEntry> entry = repository.getEntryByIdAndSecretCode(uuid,secretCode);
        if (!entry.isPresent()){
            String warnMsg = String.format("could not update entry. Entry with secretCode=%s is not found",secretCode);
            log.warn(warnMsg);
            throw new PersistentException(warnMsg);
        }else{
            pasteEntry.setUuid(entry.get().getUuid());
            pasteEntry.setCreatedOnTimestamp(entry.get().getCreatedOnTimestamp());
            repository.updateEntry(pasteEntry);
        }

    }


    @Override
    public Optional<PasteEntry> getEntry(String uuid) {
        return repository.getEntry(uuid);
    }

    @Override
    public List<PasteEntry> getPaginatedPublicEntries(int pageNumber, int limit) {
        return repository.getPaginatedPublicEntries(pageNumber, limit);
    }

    @Override
    public List<PasteEntry> getPaginatedPublicAndNotExpiredEntries(int pageNumber, int limit) {
        return repository.getPaginatedPublicAndNotExpiredEntries(pageNumber,limit);
    }

    @Override
    public boolean removeEntry(String uuid, @NonNull String secretCode)throws PersistentException{

        if (!repository.exist(uuid)){
            String warnMsg = String.format("could not removeEntry entry. Entry with uuid=%s is not found",uuid);
            log.warn(warnMsg);
            throw new PersistentException(warnMsg);
        }
        if (!repository.exist(uuid,secretCode)){
            String warnMsg = String.format("could not removeEntry entry. Entry with secretCode=%s is not found",secretCode);
            log.warn(warnMsg);
            throw new PersistentException(warnMsg);
        }else{
            return repository.removeEntry(uuid);
        }
    }

}
