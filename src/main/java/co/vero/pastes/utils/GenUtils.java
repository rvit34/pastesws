package co.vero.pastes.utils;

import java.security.SecureRandom;
import java.util.UUID;

/**
 * Created by Vitaly Romashkin on 22.04.2017.
 */
public class GenUtils {

    static final String ALHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    public static String generateSecretCode(int length){
        StringBuilder sb = new StringBuilder(length);
        for( int i = 0; i < length; i++ )
            sb.append(ALHABET.charAt( rnd.nextInt(ALHABET.length())));
        return sb.toString();
    }

    public static String generateUUID(){
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

}
