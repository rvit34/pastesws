package co.vero.pastes.utils;

import java.time.LocalDate;

/**
 * Created by Vitaly Romashkin on 22.04.2017.
 */
public class DateUtils {

    public static long daysSinceEpoch(){
        return LocalDate.now().toEpochDay();
    }

}
