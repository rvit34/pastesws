package co.vero.pastes.utils;

import io.vertx.core.json.JsonObject;
import lombok.NonNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Vitaly Romashkin on 23.04.2017.
 */
public class IOUtils {

    public static JsonObject loadConfiguration(String configFile) throws IOException{
        Path path = Paths.get(configFile);
        if (Files.notExists(path) || !Files.isReadable(path)){
            throw new IOException(String.format("file does not exist or cannot be read"));
        }
        String confContent = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
        return new JsonObject(confContent);
    }

    public static String loadContent(@NonNull InputStream is) throws IOException{
        String confContent;
        ByteArrayOutputStream baos = null;
        try {
            baos = new ByteArrayOutputStream();
            byte[] buffer  = new byte[4096];
            int bytesRead = -1;

            while ((bytesRead = is.read(buffer)) != -1) {
                baos.write(buffer,0,bytesRead);
            }

            return new String(baos.toByteArray(), StandardCharsets.UTF_8);
        } finally {
            if (baos != null){
                baos.close();
            }
            try {
                is.close();
            } catch (IOException e) {};
        }
    }


    public static JsonObject loadConfiguration(@NonNull InputStream is) throws IOException{
        return new JsonObject(loadContent(is));
    }

}
