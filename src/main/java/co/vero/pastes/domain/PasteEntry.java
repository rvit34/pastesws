package co.vero.pastes.domain;

import com.google.common.base.Objects;
import lombok.Data;

/**
 * Created by Vitaly Romashkin on 22.04.2017.
 */
@Data
public class PasteEntry {

    private String uuid;

    private String content;

    private String title;

    private boolean isPrivate;

    private String secretCode;

    private long createdOnTimestamp;

    private long expiresOnTimestamp = 0L;

    private String date; // use in cassandra partition key (ddMMyyyy)

    public boolean isExpired(){
        return expiresOnTimestamp != 0L && System.currentTimeMillis() > expiresOnTimestamp;
    }

    public boolean isNotExpired(){
        return !isExpired();
    }

    public boolean isPublic() { return !isPrivate; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PasteEntry that = (PasteEntry) o;
        return Objects.equal(uuid, that.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(uuid);
    }
}
