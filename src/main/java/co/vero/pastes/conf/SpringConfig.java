package co.vero.pastes.conf;

import co.vero.pastes.persistent.CassandraPasteEntryRepository;
import co.vero.pastes.persistent.RxPasteEntryRepository;
import co.vero.pastes.utils.IOUtils;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import io.vertx.core.json.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Vitaly Romashkin on 23.04.2017.
 */
@Configuration
@ComponentScan(basePackages = {"co.vero.pastes.persistent","co.vero.pastes.services"})
@Slf4j
public class SpringConfig {

   @Bean
   public Cluster getCassandraCluster(){
      return Cluster.builder()
              .addContactPoint("127.0.0.1")
              .withPort(9042)
              .build();
   }

   @Bean
   @Scope("prototype")
   public Session getCassandraSession(Cluster cluster){
       return cluster.connect();
   }


   @Bean(name = "CassandraRepository")
   @Scope("prototype")
   public RxPasteEntryRepository cassandraRepository(Session cassandraSession){

      JsonObject statements = loadCassandraStatements();
      Map<String,String> ddlStatements = statements
              .getJsonObject("PasteEntryDDLStatements")
              .getMap()
              .entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,entry -> entry.getValue().toString()));

      Map<String,String> preparedStatements = statements
              .getJsonObject("PasteEntryPreparedStatements")
              .getMap()
              .entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,entry -> entry.getValue().toString()));

      return new CassandraPasteEntryRepository(cassandraSession, ddlStatements, preparedStatements);
   }


   public JsonObject loadCassandraStatements(){
      String statementsFile = "cassandra_prepare_statements.json";
      try {
         return IOUtils.loadConfiguration(SpringConfig.class.getResourceAsStream("../../../../"+statementsFile));
      } catch (IOException e) {
          log.error("unable to load statements from resource {}",statementsFile,e);
          return new JsonObject();
      }
   }


}
