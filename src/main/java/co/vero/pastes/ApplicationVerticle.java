package co.vero.pastes;

import com.github.aesteve.vertx.nubes.VertxNubes;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

/**
 * Created by Vitaly Romashkin on 23.04.2017.
 */
@Slf4j
public class ApplicationVerticle extends AbstractVerticle {

    protected HttpServer server;
    protected HttpServerOptions options;
    protected VertxNubes nubes;

    private ApplicationContext springContext;

    public ApplicationVerticle(ApplicationContext springContext) {
        this.springContext = springContext;
    }

    @Override
    public void init(Vertx vertx, Context context) {
        log.info("initializing verticle...");
        super.init(vertx, context);
        JsonObject config = context.config();
        this.options = new HttpServerOptions();
        this.options.setHost(config.getString("host", "localhost"));
        this.options.setPort(config.getInteger("port", Integer.valueOf(8080)).intValue());
        this.nubes = new VertxNubes(vertx, config);
        this.nubes.registerService("PastService", springContext.getBean("PastServiceImpl"));
        log.info("verticle was initialized successfully");
    }

    @Override
    public void start(Future<Void> future) {
        log.info("starting verticle...");
        this.server = this.vertx.createHttpServer(this.options);
        log.info("bootstrapping nubes wrapper...");
        this.nubes.bootstrap(result -> {
            if (result.succeeded()){
                log.info("nubes was successfully bootstapped");
                log.info("starting http server on {}:{}...", this.options.getHost(), this.options.getPort());
                Router router = result.result();
                this.server.requestHandler(router::accept);
                // TODO: register websocket handler here this.server.websocketHandler(handler);
                this.server.listen(serverOpeningResult -> {
                    if (serverOpeningResult.succeeded()){
                        log.info("Server listening on port : " + this.options.getPort());
                        future.complete();
                    }
                    if (serverOpeningResult.failed()){
                        log.error("Failed to start server",serverOpeningResult.cause());
                        future.fail(serverOpeningResult.cause());
                    }
                });
            }
            if (result.failed()){
                log.error("could not bootstrap nubes wrapper",result.cause());
                future.fail(result.cause());
            }
        });
    }

    @Override
    public void stop(Future<Void> future) {
        log.info("stopping verticle... ");
        this.nubes.stop((nubesRes) -> {
            this.closeServer(future);
        });
    }

    private void closeServer(Future<Void> future) {
        if(this.server != null) {
            log.info("closing server...");
            this.server.close(result -> {
                if (result.succeeded()){
                    log.info("server closed");
                    future.complete();
                }
                if (result.failed()){
                    log.info("could not close server",result.cause());
                    future.fail(result.cause());
                }
            });
        } else {
            future.complete();
        }

    }
}
