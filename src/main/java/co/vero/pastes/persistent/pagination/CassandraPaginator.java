package co.vero.pastes.persistent.pagination;

import com.datastax.driver.core.*;
import com.google.common.util.concurrent.AsyncFunction;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import io.reactivex.Observable;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Vitaly Romashkin on 28.05.2017.
 */
@Slf4j
public class CassandraPaginator implements Paginator<Observable<Row>> {

    private volatile int pageSize = 10;
    private int fetchSize = pageSize*10;
    private int maxPageNumber = 100;

    private volatile PagingState pagingState = null;
    private volatile ResultSet rs;
    private volatile int currentPage = -1;
    private volatile int requestedPage = -1;

    private Session session;
    private Statement statement;

    private int cacheSize = fetchSize*10;
    private LimitedQueue<Row> cachedRows = new LimitedQueue<>(cacheSize);




    public CassandraPaginator(Session session, Statement statement, int fetchSize) {
        this.session = session;
        this.statement = statement;
        prefetch(statement,fetchSize);
    }

    private void prefetch(Statement statement, int fetchSize){
        setFetchSize(fetchSize);
        rs = session.execute(statement);
        readAvailableRowsToCache(rs);
        pagingState = rs.getExecutionInfo().getPagingState();
    }

    private void setFetchSize(int fetchSize) {
        if (fetchSize < pageSize) throw new IllegalArgumentException(String.format("fetchSize cannot be less than pageSize=%d",pageSize));
        this.fetchSize = fetchSize;
        this.statement.setFetchSize(fetchSize);
        this.cacheSize = fetchSize*10;
        this.cachedRows = new LimitedQueue<>(cacheSize);
    }


    @Override
    public Page<Observable<Row>> moveTo(int pageNumber) {

        log.debug("move to {}", pageNumber);

        if (pageNumber < 0) throw new IllegalArgumentException("page cannot be negative");
        if (pageNumber > maxPageNumber) throw new IllegalArgumentException(String.format("page cannot be greater than %d",maxPageNumber));

        if (pageNumber == currentPage){
            return currentPage();
        }

        if (currentPage + 1 == pageNumber){
            return nextPage();
        }

        if (currentPage - 1 == pageNumber) {
            return previousPage();
        }

        requestedPage = pageNumber;
        Observable<Row> observableRows = null;

        if (pageNumber > currentPage){
            observableRows = next(currentPage, Futures.immediateFuture(rs));
        }
        if (pageNumber < currentPage){
            observableRows = getRowsOfPreviousPage();
        }

/*        observableRows.doOnComplete(() -> {
            currentPage = requestedPage;
            log.debug("current page: {}", currentPage);
        });*/

        return new Page(requestedPage,pageSize,observableRows);
    }

    @Override
    public Page<Observable<Row>> nextPage() {

        Objects.requireNonNull(session);
        Objects.requireNonNull(statement);
        //Objects.requireNonNull(rs);

        if ((currentPage + 1) > maxPageNumber){
            throw new IllegalArgumentException(String.format("page cannot be greater than %d", maxPageNumber));
        }

        currentPage = currentPage + 1;
        requestedPage = currentPage;

        log.debug("nextPage: {}", currentPage);

/*        Observable<Row> observableRows;
        if (currentPage == 0 && cachedRows.size() == 0){
            observableRows = next(currentPage,session.executeAsync(statement));
        }else{
            observableRows = next(currentPage,Futures.immediateFuture(rs));
        }*/

        Observable<Row> observableRows;
        if (pageContainsInCache(currentPage)){
            log.debug("nextPage: page {} cached", currentPage);
            observableRows = Observable.fromIterable(getCachedRows(currentPage));
        }else{
            log.debug("nextPage: state {}", pagingState);
            statement.setPagingState(pagingState);
            observableRows = next(currentPage, session.executeAsync(statement));
        }


/*        observableRows.doOnComplete(() -> {
            log.debug("current page: {}", currentPage);
            currentPage = requestedPage;
        });*/

        return new Page(requestedPage,pageSize,observableRows);
    }

    @Override
    public boolean hasNextPage() {
        return pagingState != null && (currentPage + 1) <= maxPageNumber;
    }

    @Override
    public Page<Observable<Row>> previousPage() {

        Objects.requireNonNull(session);
        Objects.requireNonNull(statement);
        //Objects.requireNonNull(rs);

        if (!hasPreviousPage()){
            throw new IllegalArgumentException("previous page does not exist");
        }

        currentPage = currentPage - 1;
        requestedPage = currentPage;

        log.debug("previousPage: {}", currentPage);

        Observable<Row> observableRows = getRowsOfPreviousPage();

/*        observableRows.doOnComplete(() -> {
            currentPage = requestedPage;
            log.debug("current page: {}", currentPage);
        });*/

        return new Page(requestedPage,pageSize,observableRows);
    }

    @Override
    public boolean hasPreviousPage() {
        return (currentPage - 1) >= 0;
    }


    @Override
    public Page<Observable<Row>> currentPage(){

        if (currentPage == -1) currentPage = 0;

        Observable<Row> rowObservable;
        if (pageContainsInCache(currentPage)){
            rowObservable = Observable.fromIterable(getCachedRows(currentPage));
        }else{
            requestedPage = currentPage;
            log.debug("current page not found in cache");
            rowObservable = next(currentPage, session.executeAsync(statement));
        }

        return new Page(
                currentPage,
                pageSize, rowObservable);
    }


    @Override
    public void setPageSize(int pageSize) {
        if (pageSize <= 0) throw new IllegalArgumentException("pageSize should be positive");
        if (pageSize > fetchSize) throw new IllegalArgumentException(String.format("pageSize cannot be greater than fetchSize=%d",fetchSize));
        this.pageSize = pageSize;
    }


    private Observable<Row> getRowsOfPreviousPage() {

        Observable<Row> observableRows;
        // most likely that the requested page is already in cache
        if (pageContainsInCache(requestedPage)){
            observableRows = Observable.fromIterable(getCachedRows(requestedPage));
        }else{
            // it seems that cached rows have been deleted, so need to start at the beginning
            reinit();
            observableRows = next(currentPage,session.executeAsync(statement));
        }
        return observableRows;
    }

    public void reinit(){
        this.currentPage = -1;
        cachedRows.clear();
        cachedRows.countOfRemovedElements = 0;
        pagingState = null;
        statement.setPagingState(null);
        rs = null;
    }

    private boolean pageContainsInCache(int pageNumber){
        int fromRow = pageNumber*pageSize;
        int toRow = fromRow + pageSize;
        int cacheCount = cachedRows.size() + cachedRows.countOfRemovedElements;
        return fromRow <= cacheCount && toRow <= cacheCount && fromRow >= cachedRows.countOfRemovedElements;
    }

    private List<Row> getCachedRows(int pageNumber){

        final List<Row> pageRows = new LinkedList<>();

        final int fromRow = pageNumber*pageSize;
        final int startIndex = fromRow - cachedRows.countOfRemovedElements;
        final int endIndex = startIndex + pageSize;

        int k = 0;
        Iterator<Row> it = cachedRows.iterator();
        while(it.hasNext()){
            Row row = it.next();
            if (k >= startIndex && k < endIndex){
                pageRows.add(row);
            }
            k++;
        }

        return pageRows;
    }

    private int readAvailableRowsToCache(ResultSet rs){
        // how far we can go without triggering the blocking fetch:
        int remainingInPage = rs.getAvailableWithoutFetching(); // async operation
        log.debug("readAvailableRowsToCache: remainingInPage {}", remainingInPage);
        if (remainingInPage == 0) return 0;

        int k = remainingInPage;
        for (Row row: rs){
            cachedRows.add(row);
            if (--k == 0) break;
        }

        log.debug("readAvailableRowsToCache: cache size {}", cachedRows.size());

        return remainingInPage;
    }

    private Observable<Row> next(final int page, ListenableFuture<ResultSet> future){
        ListenableFuture<List<Row>> result = Futures.transformAsync(future,
                iterateForward(page,requestedPage));
        return Observable.fromFuture(result)
                .flatMap(rows -> Observable.fromIterable(rows));
    }


    private AsyncFunction<ResultSet,List<Row>> iterateForward(int page, final int requestedPage) {
        return new AsyncFunction<ResultSet,List<Row>>() {
            @Override
            public ListenableFuture<List<Row>> apply(ResultSet rs) throws Exception {

                //log.debug("result set: {}", rs);
                currentPage = page;
                log.debug("iterateForward: current page:{}",   currentPage);
                log.debug("iterateForward: requested page:{}", requestedPage);


                CassandraPaginator.this.rs = rs;
                int availableRows = readAvailableRowsToCache(rs);
                log.debug("iterateForward: fetched count of rows: {}", availableRows);
                pagingState = rs.getExecutionInfo().getPagingState();

                if (page == requestedPage && pageContainsInCache(page)){
                    log.debug("iterateForward: page loaded");
                    return Futures.immediateFuture(getCachedRows(page));
                }

                if (!hasNextPage() && availableRows == 0){
                    log.debug("iterateForward: we reach the end of the result set");
                    return Futures.immediateFuture(Collections.EMPTY_LIST);
                }

                // go forward
                if (pageContainsInCache(requestedPage)){
                    // no need to fetch more results if requested page is already in cache
                    return Futures.transformAsync(Futures.immediateFuture(rs),iterateForward((page+1),requestedPage));
                }else{
                    log.debug("iterateForward: fetch more results");
                    ListenableFuture<ResultSet> future = rs.fetchMoreResults(); // async operation
                    return Futures.transformAsync(future, iterateForward((page+1),requestedPage));
                }
            }
        };
    }

    /**
     * based on wait-free nonblocking implementation
     * @param <E>
     */
    private class LimitedQueue<E> extends ConcurrentLinkedQueue<E> {

        private int maxSize;

        private int countOfRemovedElements = 0;

        public LimitedQueue(int maxSize){
            this.maxSize = maxSize;
        }


        public void add(Collection<? extends E> elements) {
            elements.forEach(e -> this.add(e));
        }

        @Override
        public boolean add(E o) {
            boolean added = super.add(o);
            while (added && size() > maxSize) {
                super.remove();
                countOfRemovedElements++;
            }
            return added;
        }

    }

}
