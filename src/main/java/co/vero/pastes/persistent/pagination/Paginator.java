package co.vero.pastes.persistent.pagination;


/**
 * Created by Vitaly Romashkin on 28.05.2017.
 */
public interface Paginator<T> {

    Page<T> currentPage();

    Page<T> nextPage();

    Page<T> previousPage();

    Page<T> moveTo(int pageNumber);

    boolean hasNextPage();

    boolean hasPreviousPage();

    void setPageSize(int pageSize);

}
