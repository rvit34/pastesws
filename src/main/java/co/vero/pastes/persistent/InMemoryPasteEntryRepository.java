package co.vero.pastes.persistent;

import co.vero.pastes.domain.PasteEntry;
import lombok.NonNull;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static co.vero.pastes.utils.GenUtils.generateUUID;

/**
 * Created by Vitaly Romashkin on 22.04.2017.
 */
@Repository
public class InMemoryPasteEntryRepository implements PastEntryRepository {

    // note: usually don't need to use additional thread-safe collections and primitives due to each verticle is running in single thread
    // but for now to be sure let's usem them
    private Map<String,PasteEntry> storage = new ConcurrentHashMap<>();

    @Override
    public Optional<PasteEntry> addEntry(@NonNull PasteEntry entry) throws PersistentException {

        if(exist(entry)){
            throw new PersistentException(String.format("entry[uuid=%s] is already exist in repository",entry.getUuid()));
        }

        final String uuid = generateUUID();
        entry.setUuid(uuid);
        Object result = storage.putIfAbsent(uuid,entry);
        return Optional.ofNullable(result == null ? entry : null);
    }

    @Override
    public void updateEntry(@NonNull PasteEntry entry) throws PersistentException {
        String uuid = entry.getUuid();
        if (uuid == null || uuid.isEmpty()){
            throw new PersistentException("entry's uuid is not specified");
        }
        if (!exist(entry)){
            throw new PersistentException(String.format("entry[uuid=%s] not found in storage",entry.getUuid()));
        }
        storage.put(uuid,entry);
    }

    @Override
    public Optional<PasteEntry> getEntry(String uuid) {
        return Optional.ofNullable(storage.get(uuid));
    }

    @Override
    public Optional<PasteEntry> getEntryByIdAndSecretCode(String uuid, @NonNull String secretCode){
        final Optional<PasteEntry> entry = getEntry(uuid);
        if (entry.isPresent() && entry.get().getSecretCode().equalsIgnoreCase(secretCode)){
            return Optional.of(entry.get());
        }else{
            return Optional.empty();
        }
    }

    @Override
    public boolean exist(String uuid, String secretCode) {
        final Optional<PasteEntry> entry = getEntry(uuid);
        if (!entry.isPresent()) return false;
        return entry.get().getSecretCode().equalsIgnoreCase(secretCode);
    }

    @Override
    public boolean exist(@NonNull PasteEntry entry) {
        return exist(entry.getUuid());
    }

    @Override
    public boolean exist(String uuid) {
        return storage.containsKey(uuid);
    }

    @Override
    public boolean removeEntry(String uuid){
        return storage.remove(uuid) != null;
    }

    @Override
    public List<PasteEntry> getPaginatedPublicAndNotExpiredEntries(int pageNumber, int limit) {
        return getPaginatedPublicEntries(pageNumber,limit).stream()
                .filter(PasteEntry::isNotExpired).collect(Collectors.toList());
    }

    @Override
    public List<PasteEntry> getPaginatedPublicEntries(int pageNumber, int limit) {
        return getEntriesPaginated(pageNumber, limit, SortOrder.DESC).stream()
                .filter(PasteEntry::isPublic).collect(Collectors.toList());
    }

    @Override
    public List<PasteEntry> getEntriesPaginated(int pageNumber, int limit, @NonNull SortOrder sortOrder) {

        return getAllEntries().stream()
                 .sorted((e1, e2) -> sortOrder == SortOrder.ASC ? Long.compare(e1.getCreatedOnTimestamp(),e2.getCreatedOnTimestamp())
                     : Long.compare(e2.getCreatedOnTimestamp(), e1.getCreatedOnTimestamp()))
                 .skip(pageNumber*limit)
                 .limit(limit)
                 .collect(Collectors.toList());

    }

    private Collection<PasteEntry> getAllEntries() {
        return storage.values();
    }
}
