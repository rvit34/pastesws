package co.vero.pastes.persistent;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import lombok.NonNull;

import java.util.List;

/**
 * Created by Vitaly Romashkin on 09.05.2017.
 */
public interface RxBaseRepository<T> {

    Completable addEntry(@NonNull T entry);

    Completable addAll(@NonNull List<T> entries);

    Completable updateEntry(@NonNull T entry);

    Single<T> getEntry(String uuid);

    Single<Boolean> exist(@NonNull T entry);

    Single<Boolean> exist(String uuid);

    Completable removeEntry(String uuid);

    Observable<T> getEntriesPaginated(int pageNumber, int pageSize, @NonNull SortOrder sortOrder);

    Completable clean();

}
