package co.vero.pastes.persistent;

import co.vero.pastes.domain.PasteEntry;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import lombok.NonNull;

/**
 * Created by Vitaly Romashkin on 09.05.2017.
 */
public interface RxPasteEntryRepository extends RxBaseRepository<PasteEntry> {

    Completable removeEntry(String uuid, @NonNull String secretCode);

    Single<Boolean> exist(String uuid, @NonNull String secretCode);

    Single<PasteEntry> getEntryByIdAndSecretCode(String uuid, @NonNull String secretCode);

    Observable<PasteEntry> getPaginatedPublicEntries(int pageNumber, int pageSize);

    Observable<PasteEntry> getPaginatedPublicAndNotExpiredEntries(int pageNumber, int pageSize);

}
