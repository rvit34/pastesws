package co.vero.pastes.persistent;

import lombok.NonNull;

import java.util.List;
import java.util.Optional;

/**
 * Created by Vitaly Romashkin on 22.04.2017.
 */
public interface BaseRepository<T> {

    Optional<T> addEntry(@NonNull T entry) throws PersistentException;

    void updateEntry(@NonNull T entry) throws PersistentException;

    Optional<T> getEntry(String uuid);

    boolean exist(@NonNull T entry);

    boolean exist(String uuid);

    boolean removeEntry(String uuid) throws PersistentException;

    List<T> getEntriesPaginated(int pageNumber, int limit, @NonNull SortOrder sortOrder);

}
