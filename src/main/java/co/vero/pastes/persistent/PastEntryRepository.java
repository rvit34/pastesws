package co.vero.pastes.persistent;

import co.vero.pastes.domain.PasteEntry;
import lombok.NonNull;

import java.util.List;
import java.util.Optional;

/**
 * Created by Vitaly Romashkin on 22.04.2017.
 */
public interface PastEntryRepository extends BaseRepository<PasteEntry> {

    Optional<PasteEntry> getEntryByIdAndSecretCode(String uuid, @NonNull String secretCode);

    boolean exist(String uuid, String secretCode);

    List<PasteEntry> getPaginatedPublicEntries(int pageNumber, int limit);

    List<PasteEntry> getPaginatedPublicAndNotExpiredEntries(int pageNumber, int limit);
}
