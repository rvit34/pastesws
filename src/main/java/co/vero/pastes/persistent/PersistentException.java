package co.vero.pastes.persistent;

/**
 * Created by Vitaly Romashkin on 22.04.2017.
 */
public class PersistentException extends Exception {

    public PersistentException() {
    }

    public PersistentException(String message) {
        super(message);
    }

    public PersistentException(String message, Throwable cause) {
        super(message, cause);
    }

    public PersistentException(Throwable cause) {
        super(cause);
    }

    public PersistentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
