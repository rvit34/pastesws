package co.vero.pastes.persistent;

/**
 * Created by Vitaly Romashkin on 22.04.2017.
 */
public enum SortOrder {
    ASC,DESC;
}
