package co.vero.pastes.persistent;

import co.vero.pastes.domain.PasteEntry;
import co.vero.pastes.persistent.pagination.CassandraPaginator;
import com.datastax.driver.core.*;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Vitaly Romashkin on 09.05.2017.
 */
@Slf4j
public class CassandraPasteEntryRepository implements RxPasteEntryRepository {

    private Session session;

    private Map<String,PreparedStatement> preparedStatementMap;

    private CassandraPaginator paginator;


    public CassandraPasteEntryRepository(Session session, @NonNull Map<String,String> ddlStatements, @NonNull Map<String,String> preparedStatements) {
        this.session = session;
        executeSchema(ddlStatements);
        prepareStatements(preparedStatements);
        setConsistencyLevels();
        this.paginator = new CassandraPaginator(session,new SimpleStatement(preparedStatements.get("GET_PUBLIC_ENTRIES_DESC_ORDER")),10);
    }

    private void executeSchema(Map<String, String> ddlStatements) {
        try {
            session.execute(ddlStatements.get("CREATE_KEYSPACE"));
            session.execute(ddlStatements.get("CREATE_PASTES_SEARCH_TABLE"));
            session.execute(ddlStatements.get("CREATE_PASTES_MAIN_TABLE"));
            //session.execute(ddlStatements.get("CREATE_INDEX_ON_SECRET"));
            session.execute(ddlStatements.get("CREATE_INDEX_ON_PRIVATE"));
            log.info("cassandra schema was initialized");
        } catch (Throwable e) {
            log.error("could not execute schema",e);
            throw new RuntimeException(e);
        }
    }

    private void prepareStatements(Map<String, String> statements) {
        preparedStatementMap = statements.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> session.prepare(entry.getValue())));
        log.debug("prepared statements were loaded");
    }

    private void setConsistencyLevels(){

        // here need the serial consistency level for read requests to be enabled to see saved entries immediately (see in tests)
        preparedStatementMap.get("GET_ENTRY_BY_ID").setConsistencyLevel(ConsistencyLevel.SERIAL);
        preparedStatementMap.get("GET_ENTRY_BY_DAY_AND_CREATED_AND_ID").setConsistencyLevel(ConsistencyLevel.SERIAL);
        preparedStatementMap.get("GET_ENTRY_BY_ID_AND_SECRET").setConsistencyLevel(ConsistencyLevel.SERIAL);

        preparedStatementMap.get("GET_PUBLIC_ENTRIES_DESC_ORDER").setConsistencyLevel(ConsistencyLevel.SERIAL);

        //preparedStatementMap.get("TRUNCATE_SEARCH_TABLE").setConsistencyLevel(ConsistencyLevel.SERIAL);
        //preparedStatementMap.get("TRUNCATE_MAIN_TABLE").setConsistencyLevel(ConsistencyLevel.SERIAL);


    }


    @Override
    public Completable addEntry(@NonNull PasteEntry entry) {

        log.debug("addEntry: {}", entry.toString());

        final LocalDate localDate = LocalDate.fromMillisSinceEpoch(entry.getCreatedOnTimestamp());
        final UUID uuid = UUID.fromString(entry.getUuid());

        BatchStatement insertBatchStatement = new BatchStatement(BatchStatement.Type.LOGGED);
        addToInsertBatchStatement(entry, localDate, uuid, insertBatchStatement);

        return executeStatement(insertBatchStatement);
    }


    @Override
    public Completable addAll(List<PasteEntry> entries) {

        log.debug("addEntries: size = {}", entries.size());

        LocalDate localDate = null;

        BatchStatement insertBatchStatement = new BatchStatement(BatchStatement.Type.LOGGED);
        UUID uuid;

        for (PasteEntry entry : entries) {

            localDate = LocalDate.fromMillisSinceEpoch(entry.getCreatedOnTimestamp());
            uuid = UUID.fromString(entry.getUuid());

            addToInsertBatchStatement(entry, localDate, uuid, insertBatchStatement);
        }

        return executeStatement(insertBatchStatement);
    }

    private void addToInsertBatchStatement(@NonNull PasteEntry entry, LocalDate localDate, UUID uuid, BatchStatement insertBatchStatement) {
        insertBatchStatement.add(preparedStatementMap.get("INSERT_NEW_ENTRY_INTO_SEARCH_TABLE").bind(
                uuid,
                entry.getSecretCode(),
                entry.getCreatedOnTimestamp()
        ));
        insertBatchStatement.add(preparedStatementMap.get("INSERT_NEW_ENTRY_INTO_MAIN_TABLE").bind(
                localDate,
                uuid,
                entry.getSecretCode(),
                entry.getCreatedOnTimestamp(),
                entry.isPrivate(),
                entry.getExpiresOnTimestamp(),
                entry.getContent(),
                (entry.getTitle() == null ? "" : entry.getTitle()) // cause it's optional
        ));
    }


    @Override
    public Completable updateEntry(@NonNull PasteEntry entry) {
        log.debug("updateEntry: {}", entry.toString());
        return getEntryByIdAndSecretCode(entry.getUuid(),entry.getSecretCode()) // first search by uuid and secret code
                .flatMapCompletable(dbEntry -> {
                    Statement updateStatement = preparedStatementMap.get("UPDATE_ENTRY_IN_MAIN_TABLE").bind(  // then do update
                            // update params
                            entry.isPrivate(),
                            entry.getContent(),
                            (entry.getTitle() == null ? "" : entry.getTitle()), // cause it's optional
                            entry.getExpiresOnTimestamp(),
                            // where params
                            LocalDate.fromMillisSinceEpoch(dbEntry.getCreatedOnTimestamp()),
                            dbEntry.getCreatedOnTimestamp(),
                            UUID.fromString(dbEntry.getUuid())
                    );
                    return executeStatement(updateStatement);
                });
    }

    @Override
    public Completable removeEntry(@NonNull String uuid) {
        log.debug("removeEntry by uuid {}", uuid);
        return getEntry(uuid) // first search by uuid
               .flatMapCompletable(dbEntry -> executeStatement(createDeleteBatchStatement(dbEntry)));
    }

    @Override
    public Completable removeEntry(String uuid, String secretCode) {
        log.debug("removeEntry by uuid {} and secretCode {}", uuid, secretCode);
        return getEntryByIdAndSecretCode(uuid,secretCode) // first search by uuid and secret
                .flatMapCompletable(dbEntry -> executeStatement(createDeleteBatchStatement(dbEntry)));
    }

    private BatchStatement createDeleteBatchStatement(PasteEntry dbEntry) {
        BatchStatement deleteBatchStatement = new BatchStatement(BatchStatement.Type.LOGGED);
        deleteBatchStatement.add(preparedStatementMap.get("DELETE_ENTRY_IN_SEARCH_TABLE").bind(UUID.fromString(dbEntry.getUuid())));
        deleteBatchStatement.add(preparedStatementMap.get("DELETE_ENTRY_IN_MAIN_TABLE").bind(
                LocalDate.fromMillisSinceEpoch(dbEntry.getCreatedOnTimestamp()),
                dbEntry.getCreatedOnTimestamp(),
                UUID.fromString(dbEntry.getUuid())
        ));
        return deleteBatchStatement;
    }


    @Override
    public Single<PasteEntry> getEntry(String uuid) {
        log.debug("getEntry by uuid={}", uuid);
        return getSingleResultSet(preparedStatementMap.get("GET_ENTRY_BY_ID").bind(UUID.fromString(uuid))) // first search entry in search table
            .map(rows -> {
                Row one = rows.one();
                if (one == null){
                    throw new PersistentException(String.format("entry with uuid=%s is not found in search table",uuid));
                }
                return one;
            })
            .map(row -> mapToPastEntry().apply(row))
            .flatMap(pasteEntry -> getEntry(pasteEntry.getUuid(), pasteEntry.getCreatedOnTimestamp())); // then search entry with date in main table
    }

    @Override
    public Single<PasteEntry> getEntryByIdAndSecretCode(String uuid, String secret){
        log.debug("getEntry by uuid={} and secret={}", uuid, secret);
        return getSingleResultSet(preparedStatementMap.get("GET_ENTRY_BY_ID_AND_SECRET").bind(
                                   UUID.fromString(uuid), secret.trim())) // first search entry in search table
                .map(rows -> {
                    Row one = rows.one();
                    if (one == null){
                        throw new PersistentException(String.format("entry with uuid=%s and secret=%s is not found in search table",uuid,secret));
                    }
                    return one;
                })
                .map(row -> mapToPastEntry().apply(row))
                .flatMap(pasteEntry -> getEntry(pasteEntry.getUuid(), pasteEntry.getCreatedOnTimestamp())); // then search entry with date in main table
    }


    private Single<PasteEntry> getEntry(String uuid, long created){
        return getSingleResultSet(preparedStatementMap.get("GET_ENTRY_BY_DAY_AND_CREATED_AND_ID").bind(
                LocalDate.fromMillisSinceEpoch(created), created, UUID.fromString(uuid)))
                .map(rows -> {
                    Row one = rows.one();
                    if (one == null){
                        throw new PersistentException(String.format("entry with uuid=%s and created=%s is not found in main tale",uuid,created));
                    }
                    return one;
                })
                .map(row -> mapToCompletePastEntry().apply(row));
    }

    @Override
    public Single<Boolean> exist(@NonNull PasteEntry entry) {
        return exist(entry.getUuid());
    }

    @Override
    public Single<Boolean> exist(String uuid) {
        return getEntry(uuid).map(pasteEntry -> pasteEntry != null && pasteEntry.getUuid().equalsIgnoreCase(uuid));                            //.isEmpty().map(bool -> !bool.booleanValue());
    }

    @Override
    public Completable clean() {

        log.debug("clean");

        final Statement truncateSearchTable = preparedStatementMap.get("TRUNCATE_SEARCH_TABLE").bind();
        final Statement truncateMainTable = preparedStatementMap.get("TRUNCATE_MAIN_TABLE").bind();

        return executeStatement(truncateSearchTable).andThen(executeStatement(truncateMainTable));
    }

    @Override
    public Single<Boolean> exist(String uuid, @NonNull String secret) {
        return getEntryByIdAndSecretCode(uuid,secret).map(pasteEntry -> pasteEntry != null && pasteEntry.getUuid().equalsIgnoreCase(uuid));                   //isEmpty().map(bool -> !bool.booleanValue());
    }


    @Override
    public Observable<PasteEntry> getEntriesPaginated(int pageNumber, int pageSize, @NonNull SortOrder sortOrder) {
        return null;
    }


    @Override
    public Observable<PasteEntry> getPaginatedPublicEntries(int pageNumber, int pageSize) {

        paginator.setPageSize(pageSize);
        return paginator.moveTo(pageNumber)
                .getContent()
                .map(row -> mapToCompletePastEntry().apply(row));
    }

    @Override
    public Observable<PasteEntry> getPaginatedPublicAndNotExpiredEntries(int pageNumber, int pageSize) {
        return getPaginatedPublicEntries(pageNumber,pageSize).filter(PasteEntry::isNotExpired);
    }


    private Observable<ResultSet> getObservableResultSet(String cql){
        final ResultSetFuture resultSetFuture = session.executeAsync(cql);
        return Observable.fromFuture(resultSetFuture);
    }

    private Observable<ResultSet> getObservableResultSet(Statement statement){
        final ResultSetFuture resultSetFuture = session.executeAsync(statement);
        return Observable.fromFuture(resultSetFuture);
    }

    private Single<ResultSet> getSingleResultSet(String cql){
        final ResultSetFuture resultSetFuture = session.executeAsync(cql);
        return Single.fromFuture(resultSetFuture);
    }

    private Single<ResultSet> getSingleResultSet(Statement statement){
        final ResultSetFuture resultSetFuture = session.executeAsync(statement);
        return Single.fromFuture(resultSetFuture);
    }

    private Maybe<ResultSet> getMaybeResultSet(Statement statement){
        final ResultSetFuture resultSetFuture = session.executeAsync(statement);
        return Maybe.fromFuture(resultSetFuture);
    }

    private Completable executeStatement(Statement statement){
        return Completable.fromFuture(session.executeAsync(statement));
    }

    private Function<Row,PasteEntry> mapToPastEntry(){
        return row -> {
            log.debug("mapToPastEntry: row={}",row == null? "null":row.toString());
            PasteEntry pasteEntry = new PasteEntry();
            pasteEntry.setUuid(row.getUUID("id").toString());
            pasteEntry.setCreatedOnTimestamp(row.getTime("created"));
            pasteEntry.setSecretCode(row.getString("secret"));
            return pasteEntry;
        };
    }

    private Function<Row,PasteEntry> mapToCompletePastEntry(){
        return row -> {
            log.debug("mapToCompletePastEntry: row={}", row == null? "null":row.toString());
            PasteEntry pasteEntry = new PasteEntry();
            pasteEntry.setUuid(row.getUUID("id").toString());
            pasteEntry.setDate(row.getDate("day").toString());
            pasteEntry.setCreatedOnTimestamp(row.getTime("created"));
            pasteEntry.setSecretCode(row.getString("secret"));
            pasteEntry.setTitle(row.getString("title"));
            pasteEntry.setContent(row.getString("content"));
            pasteEntry.setExpiresOnTimestamp(row.getTime("expires"));
            pasteEntry.setPrivate(row.getBool("private"));

            return pasteEntry;
        };
    }

}
