package co.vero.pastes.api.dto;

import lombok.Data;

/**
 * Created by Vitaly Romashkin on 24.04.2017.
 */
@Data
public class PastEntryResponseDTO {

    private String uuid;
    private String title;
    private String body;
    private long created;
    private long expired;

}
