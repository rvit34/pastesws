package co.vero.pastes.api.dto;


import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by Vitaly Romashkin on 23.04.2017.
 */
@Data
public class PageDTO {

    @NotNull(message = "param 'pageNumber' is not specified")
    @Min(value = 0,message = "param 'pageNumber' should be positive")
    private Integer pageNumber;

    @NotNull(message = "param 'limit' is not specified")
    @Min(value = 0,message = "param 'limit' should be positive")
    private Integer limit;
}
