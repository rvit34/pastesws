package co.vero.pastes.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * Created by Vitaly Romashkin on 24.04.2017.
 */
@Data
public class PastEntryRequestDTO
{
    @NotNull(message = "param 'content' cannot be null")
    @NotEmpty(message = "param 'content' cannot be empty")
    private String body;

    private String title;

    private long expires;
    
    @JsonProperty("private")
    private boolean isPrivate;
    
}
