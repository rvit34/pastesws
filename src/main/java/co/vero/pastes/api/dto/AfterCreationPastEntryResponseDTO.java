package co.vero.pastes.api.dto;

import lombok.Data;

/**
 * Created by Vitaly Romashkin on 24.04.2017.
 */
@Data
public class AfterCreationPastEntryResponseDTO {

    private String uuid;
    private String secretCode;
    private long created;

}
