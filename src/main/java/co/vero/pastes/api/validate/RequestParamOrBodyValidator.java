package co.vero.pastes.api.validate;

import com.github.aesteve.vertx.nubes.exceptions.ValidationException;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;


/**
 * Created by Vitaly Romashkin on 24.03.2017.
 */
public interface RequestParamOrBodyValidator<T> {
    
    static void validate(Validator validator, Object objUnderValidation) throws ValidationException{

        Set<ConstraintViolation<Object>> violations = validator.validate(objUnderValidation);
        if (!violations.isEmpty()){
            final StringBuilder errorMsgBuilder = new StringBuilder();
            violations.forEach(violation -> {
                 errorMsgBuilder.append(String.format("error in param: %s;",violation.getMessage()));
            });
            throw new ValidationException(errorMsgBuilder.toString());
        }
    }
}