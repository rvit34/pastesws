package co.vero.pastes.api.ex;

import com.github.aesteve.vertx.nubes.exceptions.http.HttpException;

/**
 * Created by Vitaly Romashkin on 24.04.2017.
 */
public class UnauthorizedRequestException extends HttpException {

    protected UnauthorizedRequestException(int status, String msg) {
        super(status, msg);
    }

    public UnauthorizedRequestException(String msg){
        this(401,msg);
    }
}
