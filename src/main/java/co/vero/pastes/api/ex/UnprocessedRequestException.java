package co.vero.pastes.api.ex;

import com.github.aesteve.vertx.nubes.exceptions.http.HttpException;

/**
 * Created by Vitaly Romashkin on 24.04.2017.
 */
public class UnprocessedRequestException extends HttpException {

    protected UnprocessedRequestException(int status, String msg) {
        super(status, msg);
    }
    public UnprocessedRequestException(String msg){
        this(409,msg);
    }
}
