package co.vero.pastes.api.controllers;

import co.vero.pastes.api.dto.AfterCreationPastEntryResponseDTO;
import co.vero.pastes.api.dto.PageDTO;
import co.vero.pastes.api.dto.PastEntryRequestDTO;
import co.vero.pastes.api.dto.PastEntryResponseDTO;
import co.vero.pastes.api.ex.UnauthorizedRequestException;
import co.vero.pastes.api.ex.UnprocessedRequestException;
import co.vero.pastes.api.validate.RequestParamOrBodyValidator;
import co.vero.pastes.domain.PasteEntry;
import co.vero.pastes.persistent.PersistentException;
import co.vero.pastes.services.PastService;
import com.github.aesteve.vertx.nubes.annotations.Controller;
import com.github.aesteve.vertx.nubes.annotations.mixins.ContentType;
import com.github.aesteve.vertx.nubes.annotations.params.Param;
import com.github.aesteve.vertx.nubes.annotations.params.Params;
import com.github.aesteve.vertx.nubes.annotations.params.RequestBody;
import com.github.aesteve.vertx.nubes.annotations.routing.http.DELETE;
import com.github.aesteve.vertx.nubes.annotations.routing.http.GET;
import com.github.aesteve.vertx.nubes.annotations.routing.http.POST;
import com.github.aesteve.vertx.nubes.annotations.routing.http.PUT;
import com.github.aesteve.vertx.nubes.annotations.services.Service;
import com.github.aesteve.vertx.nubes.exceptions.ValidationException;
import com.github.aesteve.vertx.nubes.exceptions.http.HttpException;
import com.github.aesteve.vertx.nubes.exceptions.http.impl.BadRequestException;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import lombok.extern.slf4j.Slf4j;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Vitaly Romashkin on 23.04.2017.
 */
@Controller("/entries")
@Slf4j
public class RestPastesController {

    private ValidatorFactory validatorFactory =  Validation.buildDefaultValidatorFactory();

    public RestPastesController() {
       log.debug("RestPastController created");
    }

    @Service("PastService")
    private PastService pastService;

    @GET
   @ContentType("application/json")
    public void entries(HttpServerResponse response, @Params PageDTO page) throws BadRequestException{

        try {
            RequestParamOrBodyValidator.validate(getValidator(),page);
        } catch (ValidationException e) {
            log.warn("entries request does not pass the validation step: {}",e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

         List<PastEntryResponseDTO> responseBody = pastService.getPaginatedPublicAndNotExpiredEntries(page.getPageNumber(), page.getLimit()).stream()
                 .map(this::convertToResponseDto)
                 .collect(Collectors.toList());

         response
                 .setStatusCode(200)
                 .putHeader("content-type","application/json; charset=utf-8")
                 .end(Json.encodePrettily(responseBody));
    }

    @POST
    @ContentType("application/json")
    public void addEntry(HttpServerResponse response, @RequestBody PastEntryRequestDTO pastEntryDTO) throws HttpException {

        try {
            RequestParamOrBodyValidator.validate(getValidator(),pastEntryDTO);
        } catch (ValidationException e) {
            log.warn("add new PastEntry request does not pass the validation step: {}",e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        try {
            Optional<PasteEntry> result = pastService.addEntry(convertToPastEntry(pastEntryDTO));
            if (!result.isPresent()){
                throw new UnprocessedRequestException("Cannot add entry right now. Try again later");
            }else {
                AfterCreationPastEntryResponseDTO responseBody = convertToResponseDtoAfterCreation(result.get());
                response
                        .setStatusCode(201)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(responseBody));
                // TODO: publish to websocket clients
            }
        } catch (PersistentException e) {
            throw new UnprocessedRequestException(e.getMessage());
        }

    }



    @PUT("/:entry_id")
    @ContentType("application/json")
    public void updateEntry(HttpServerResponse response, @Param("entry_id") String entryId, @Param("secret") String secret, @RequestBody PastEntryRequestDTO pastEntryDTO) throws HttpException {

        if (entryId == null || entryId.isEmpty()){
           throw new BadRequestException("entry uuid is not specified");
        }

        if (secret == null || secret.isEmpty()) {
            String warnMsg = "param 'secret' is not specified";
            log.warn(warnMsg);
            throw new UnauthorizedRequestException(warnMsg);
        }

        try {
            RequestParamOrBodyValidator.validate(getValidator(),pastEntryDTO);
        } catch (ValidationException e) {
            log.warn("update PastEntry request does not pass the validation step: {}",e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        try {
            pastService.updateEntry(entryId,secret,convertToPastEntry(pastEntryDTO));
            response
                    .setStatusCode(200)
                    .end();

        } catch (PersistentException e) {
            throw new UnprocessedRequestException(e.getMessage());
        }
    }


    @DELETE("/:entry_id")
    public void removeEntry(HttpServerResponse response, @Param("entry_id") String entryId, @Param("secret") String secret) throws HttpException {

        if (entryId == null || entryId.isEmpty()){
            throw new BadRequestException("entry uuid is not specified");
        }

        if (secret == null || secret.isEmpty()) {
            String warnMsg = "param 'secret' is not specified";
            log.warn(warnMsg);
            throw new UnauthorizedRequestException(warnMsg);
        }

        try {
            pastService.removeEntry(entryId,secret);
            response
                    .setStatusCode(200)
                    .end();

        } catch (PersistentException e) {
            throw new UnprocessedRequestException(e.getMessage());
        }
    }



    private PasteEntry convertToPastEntry(PastEntryRequestDTO pastEntryRequestDTO) {
        PasteEntry pasteEntry = new PasteEntry();
        pasteEntry.setContent(pastEntryRequestDTO.getBody());
        pasteEntry.setPrivate(pastEntryRequestDTO.isPrivate());
        pasteEntry.setExpiresOnTimestamp(pastEntryRequestDTO.getExpires());
        pasteEntry.setTitle(pastEntryRequestDTO.getTitle());

        return pasteEntry;
    }

    private AfterCreationPastEntryResponseDTO convertToResponseDtoAfterCreation(PasteEntry pastEntry){
        AfterCreationPastEntryResponseDTO response = new AfterCreationPastEntryResponseDTO();
        response.setUuid(pastEntry.getUuid());
        response.setSecretCode(pastEntry.getSecretCode());
        response.setCreated(pastEntry.getCreatedOnTimestamp());

        return response;
    }

    private PastEntryResponseDTO convertToResponseDto(PasteEntry pastEntry){

        PastEntryResponseDTO response = new PastEntryResponseDTO();
        response.setUuid(pastEntry.getUuid());
        response.setTitle(pastEntry.getTitle());
        response.setBody(pastEntry.getContent());
        response.setCreated(pastEntry.getCreatedOnTimestamp());
        response.setExpired(pastEntry.getExpiresOnTimestamp());

        return response;
    }


    private Validator getValidator(){
        return validatorFactory.getValidator();
    }


}
