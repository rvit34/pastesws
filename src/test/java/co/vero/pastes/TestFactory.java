package co.vero.pastes;

import co.vero.pastes.domain.PasteEntry;
import co.vero.pastes.persistent.CassandraPasteEntryRepository;
import co.vero.pastes.utils.GenUtils;
import co.vero.pastes.utils.IOUtils;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import io.reactivex.Emitter;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.vertx.core.json.JsonObject;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by Vitaly Romashkin on 10.05.2017.
 */
@Slf4j
public class TestFactory {

    public Cluster createCassandraTestCluster(){
        return Cluster.builder()
                .addContactPoint("127.0.0.1")
                .withPort(9042)
                .build();
    }

    private JsonObject loadCassandraStatements(){
        String statementsFile = "cassandra_prepare_statements_test.json";
        try {
            return IOUtils.loadConfiguration(TestFactory.class.getResourceAsStream("../../../"+statementsFile));
        } catch (IOException e) {
            log.error("unable to load statements from resource {}",statementsFile,e);
            return new JsonObject();
        }
    }

    public Map<String,String> loadCassandraQueryStatementsMap(){
        JsonObject statementsJson = loadCassandraStatements();
        return statementsJson.getJsonObject("PasteEntryPreparedStatements")
                .getMap().entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().toString()));
    }

    public CassandraPasteEntryRepository getCassandraTestRepository(Session session){

        JsonObject statements = loadCassandraStatements();
        Map<String,String> ddlStatements = statements
                .getJsonObject("PasteEntryDDLStatements")
                .getMap()
                .entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().toString()));

        Map<String,String> preparedStatements = statements
                .getJsonObject("PasteEntryPreparedStatements")
                .getMap()
                .entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,entry -> entry.getValue().toString()));


        return new CassandraPasteEntryRepository(session,ddlStatements,preparedStatements);
    }


    public PasteEntry createTestPasteEntry(boolean isPrivate, boolean isExpired){

        PasteEntry entry = new PasteEntry();
        entry.setPrivate(isPrivate);
        entry.setExpiresOnTimestamp(isExpired?System.currentTimeMillis()-10*1000:System.currentTimeMillis()+100*1000);
        entry.setTitle("Test entry");
        entry.setContent("Test: Hello,World!");
        entry.setCreatedOnTimestamp(System.currentTimeMillis());
        entry.setUuid(GenUtils.generateUUID());
        entry.setSecretCode(GenUtils.generateSecretCode(64));

        return entry;
    }


    public List<PasteEntry> generateTestEntries(boolean isPrivate, boolean isExpired, int count, int delay){

        return Observable
                .generate((Consumer<Emitter<PasteEntry>>) pasteEntryEmitter -> {
                    pasteEntryEmitter.onNext(TestFactory.this.createTestPasteEntry(isPrivate, isExpired));
                    pasteEntryEmitter.onComplete();
                })
                .delay(delay, TimeUnit.MILLISECONDS)
                .repeat(count)
                .toList()
                .blockingGet();

    }

}
