package co.vero.pastes.persistent;

import co.vero.pastes.TestFactory;
import co.vero.pastes.domain.PasteEntry;
import com.datastax.driver.core.Session;
import com.google.common.collect.Lists;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import io.reactivex.subscribers.TestSubscriber;
import lombok.extern.slf4j.Slf4j;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;

/**
 * Created by Vitaly Romashkin on 10.05.2017.
 */
@RunWith(JUnit4.class)
@Slf4j
public class CassandraPasteEntryRepositoryTest {

    private static CassandraPasteEntryRepository repository;
    private static TestFactory testFactory;
    private static Session session;


    @BeforeClass
    public static void setUp() throws Exception{
        testFactory = new TestFactory();
        session = testFactory.createCassandraTestCluster().connect();
        repository = testFactory.getCassandraTestRepository(session);
    }

    @Test
    public void testThatEntryWasAddedSuccessfully(){
        // given:
        PasteEntry testEntry = testFactory.createTestPasteEntry(false,false);
        TestObserver<PasteEntry> observer = new TestObserver<>();
        final Completable result = repository.addEntry(testEntry);

        // when:
        result.subscribe(observer);

        //then:
        observer.assertComplete();
        observer.assertNoErrors();
    }

    @Test
    public void testThatAllEntriesWereAddedSuccessfully(){
        // given:
        final List<PasteEntry> testEntries = testFactory.generateTestEntries(false, false, 3, 10);

        final Flowable<Boolean> result = Single.merge(
                repository.addAll(testEntries).andThen(repository.exist(testEntries.get(0))),
                repository.exist(testEntries.get(1)),
                repository.exist(testEntries.get(2))
        );

        // when:
        final TestSubscriber<Boolean> observer = result.test();

        //then:
        observer.assertComplete();
        observer.assertNoErrors();
        observer.assertValues(true,true,true);
    }

    @Test
    public void testThatEntryCanBeRetrieved(){
        // given:
        PasteEntry testEntry = testFactory.createTestPasteEntry(false,false);


        final Single<PasteEntry> result = repository
                                                .addEntry(testEntry)
                                                .andThen(repository.getEntry(testEntry.getUuid()));

        // when:
        final TestObserver<PasteEntry> observer = result.test();

        //then:
        observer.assertComplete();
        observer.assertNoErrors();
        observer.assertValueCount(1);

        Assert.assertThat(observer.values().get(0).getUuid(), is(testEntry.getUuid()));

    }


    @Test
    public void testThatEntryWasUpdatedSuccessfully(){

        // given:
        final PasteEntry testEntry = testFactory.createTestPasteEntry(false,false);
        final TestObserver<PasteEntry> observer = new TestObserver<>();

        final Single<PasteEntry> result = repository
                .addEntry(testEntry)
                .doOnComplete(() -> testEntry.setContent("Test: Hello,New World!"))
                .andThen(repository.updateEntry(testEntry))
                .andThen(repository.getEntry(testEntry.getUuid()));

        // when:
         result.subscribe(observer);


        //then:
        observer.assertComplete();
        observer.assertNoErrors();
        observer.assertValueCount(1);

        Assert.assertThat(observer.values().get(0).getContent(), is("Test: Hello,New World!"));

    }


    @Test
    public void testThatEntryWasDeleted(){

        // given:
        final PasteEntry testEntry = testFactory.createTestPasteEntry(false,false);
        final Single result = repository
                .addEntry(testEntry)
                .andThen(repository.removeEntry(testEntry.getUuid(), testEntry.getSecretCode()))
                .andThen(repository.getEntry(testEntry.getUuid()));

        // when:
        final TestObserver observer = result.test();


        //then:
        observer.assertNotComplete();
        observer.assertError(PersistentException.class);

    }

    @Test
    public void testThatEntryExist(){

        // given:
        final PasteEntry testEntry = testFactory.createTestPasteEntry(false,false);

        Flowable<Boolean> exist = Single.merge(
                                   repository.addEntry(testEntry).andThen(repository.exist(testEntry.getUuid())),
                                   repository.exist(testEntry),
                                   repository.exist(testEntry.getUuid(), testEntry.getSecretCode())
        );

        // when:
        final TestSubscriber<Boolean> subscriber = exist.test();

        // then:
        subscriber.assertComplete();
        subscriber.assertNoErrors();
        subscriber.assertValues(true,true,true);

    }

    @Test
    public void testThatRepositoryWasCleaned(){
        // given:
        final List<PasteEntry> entries = testFactory.generateTestEntries(false, false, 5, 10);
        final Completable result = repository.addAll(entries).andThen(repository.clean());

        // when:
        final TestObserver<Void> observer = result.test();

        // then:
        observer.assertComplete();
        observer.assertNoErrors();
    }


    @Test
    public void testGettingAListOfEntries(){

        // given:
        List<PasteEntry> privateEntries = testFactory.generateTestEntries(true, false, 30, 10);
        List<PasteEntry> publicAndNotExpiredEntries = testFactory.generateTestEntries(false, false, 40, 10);
        List<PasteEntry> expiredEntries = testFactory.generateTestEntries(false, true, 30, 10);

        List<PasteEntry> entries = new ArrayList<>();
        entries.addAll(privateEntries);
        entries.addAll(publicAndNotExpiredEntries);
        entries.addAll(expiredEntries);

        log.debug("entries:");
        entries.forEach(v -> log.debug(v.getUuid()+" "+v.getCreatedOnTimestamp()+"  "+v.isPrivate()));

        Observable<PasteEntry> result = repository
                                                .addAll(entries)
                                                .andThen(
                                                        Observable.merge(
                                                                Observable.fromCallable(() -> repository.getPaginatedPublicEntries(0, 10)),
                                                                Observable.fromCallable(() -> repository.getPaginatedPublicEntries(1, 10)),
                                                                Observable.fromCallable(() -> repository.getPaginatedPublicEntries(2, 10)),
                                                                Observable.fromCallable(() -> repository.getPaginatedPublicEntries(3, 10))
                                                        )
                                                ).flatMap(pasteEntryObservable -> pasteEntryObservable);

        // when:
        TestObserver<PasteEntry> observer = result.test();

        // then:
        observer.assertComplete();
        observer.assertNoErrors();

        log.debug("expected:");
        Lists.reverse(publicAndNotExpiredEntries).forEach(v -> log.debug(v.getUuid()+" "+v.getCreatedOnTimestamp()));

        log.debug("actual:");
        observer.values().forEach(v -> log.debug(v.getUuid()+" "+v.getCreatedOnTimestamp()));

        observer.assertValues(Lists.reverse(publicAndNotExpiredEntries).toArray(new PasteEntry[40]));

    }

    @AfterClass
    public static void cleanUp(){
        final Completable completable = repository.clean().doFinally(() -> {
            if (session != null) {
                session.close();
                session.getCluster().close();
            }
        });
    }


}