package co.vero.pastes.persistent.pagination;

import co.vero.pastes.TestFactory;
import co.vero.pastes.domain.PasteEntry;
import co.vero.pastes.persistent.CassandraPasteEntryRepository;
import com.datastax.driver.core.*;
import com.google.common.collect.Lists;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;


/**
 * Created by Vitaly Romashkin on 29.05.2017.
 */
@RunWith(JUnit4.class)
@Slf4j
public class CassandraPaginatorTest {

    private static CassandraPasteEntryRepository repository;
    private static TestFactory testFactory;
    private static Cluster cluster;
    private static Session session;


    private static CassandraPaginator paginator;
    private static Map<String,String> statements;

    @BeforeClass
    public static void setUp() throws Exception{

        testFactory = new TestFactory();
        cluster = testFactory.createCassandraTestCluster();
        session = cluster.connect();
        repository = testFactory.getCassandraTestRepository(session);
        statements = testFactory.loadCassandraQueryStatementsMap();

        Statement statement = new SimpleStatement(statements.get("GET_PUBLIC_ENTRIES_DESC_ORDER"));
        //statement.setConsistencyLevel(ConsistencyLevel.SERIAL);

        paginator = new CassandraPaginator(session,statement,10);
    }


    @Test
    public void testThatCurrentPageIsGettingCorrectly(){

        // given:
        paginator.setPageSize(5);
        final List<PasteEntry> testEntries = testFactory.generateTestEntries(false, false, 10, 1);

        final Observable<PasteEntry> result = toObservable(repository
                                                                .addAll(testEntries)
                                                                .andThen(Single.fromCallable(paginator::currentPage)));

        // when:
        TestObserver observer = result.test();

        // then:
        observer.assertComplete();
        observer.assertNoErrors();

        observer.values().stream().forEach(System.out::println);

        observer.assertValueCount(5);
        observer.assertValues(Lists.reverse(testEntries.subList(5,10)).toArray(new PasteEntry[5]));

    }


    @Test
    public void testForwardPagination(){

        // given:
        paginator.setPageSize(5);
        final List<PasteEntry> testEntries = testFactory.generateTestEntries(false, false, 15, 1);
        testEntries.stream().forEach(System.out::println);

        final Observable<PasteEntry> result = toObservable(repository
                                                                .addAll(testEntries)
                                                                .andThen(Single.concat(
                                                                        Single.fromCallable(paginator::nextPage),
                                                                        Single.fromCallable(paginator::nextPage),
                                                                        Single.fromCallable(paginator::nextPage)
                                                                        )
                                                                ));

        // when:
        TestObserver observer = result.test();

        observer.values().stream().forEach(System.out::println);

        //then:
        observer.assertComplete();
        observer.assertNoErrors();

        observer.assertValueCount(15);
        observer.assertValues(Lists.reverse(testEntries.subList(0,15)).toArray(new PasteEntry[15]));

    }

    @Test
    public void testBackwardPagination(){

        // given:
        paginator.setPageSize(3);
        final List<PasteEntry> testEntries = testFactory.generateTestEntries(false, false, 15, 1);

        Observable<PasteEntry> result = toObservable(repository
                                                        .addAll(testEntries)
                                                        .andThen(
                                                                Single.concat(
                                                                        Single.fromCallable(paginator::nextPage),
                                                                        Single.fromCallable(paginator::nextPage),
                                                                        Single.fromCallable(paginator::nextPage),
                                                                        Single.fromCallable(paginator::previousPage)
                                                                )
                                                        )
                                        ).takeLast(3);

        // when:
        TestObserver<PasteEntry> observer = result.test();

        // then:
        observer.assertComplete();
        observer.assertNoErrors();

        observer.assertValueCount(3);
        observer.assertValues(Lists.reverse(testEntries.subList(9,12)).toArray(new PasteEntry[3]));

    }

    @Test
    public void testArbitraryPagination(){

        // given:
        paginator.setPageSize(10);
        // here we remember that cache size is 100
        final List<PasteEntry> testEntries = Lists.reverse(testFactory.generateTestEntries(false, false, 110, 1));

        List<PasteEntry> expectedOn0thPage = testEntries.subList(0,10);
        List<PasteEntry> expectedOn5thPage = testEntries.subList(50,60);
        List<PasteEntry> expectedOn10thPage = testEntries.subList(100,110);

        log.debug("expected values:");
        log.debug("0th:");
        expectedOn0thPage.stream().forEach(v->log.debug(v.toString()));
        log.debug("5th:");
        expectedOn5thPage.stream().forEach(v->log.debug(v.toString()));
        log.debug("10th:");
        expectedOn10thPage.stream().forEach(v->log.debug(v.toString()));

        List<PasteEntry> expectedValues = new LinkedList<>();
        expectedValues.addAll(expectedOn0thPage);
        expectedValues.addAll(expectedOn5thPage);
        expectedValues.addAll(expectedOn10thPage);
        expectedValues.addAll(expectedOn5thPage);
        expectedValues.addAll(expectedOn0thPage);


        Observable<PasteEntry> result = toObservable(repository
                                                        .addAll(testEntries)
                                                        .andThen(Single.concatArray(
                                                                Single.fromCallable(() -> paginator.moveTo(0)),
                                                                Single.fromCallable(() -> paginator.moveTo(5)),
                                                                Single.fromCallable(() -> paginator.moveTo(10)),
                                                                Single.fromCallable(() -> paginator.moveTo(5)),
                                                                Single.fromCallable(() -> paginator.moveTo(0))
                                                                )
                                                        ));

        // when:
        TestObserver<PasteEntry> observer = result.test();

        // then:
        observer.assertComplete();
        observer.assertNoErrors();

        List<PasteEntry> actualValues = observer.values();

        log.debug("actual values:");
        log.debug("0th:");
        actualValues.subList(0,10).stream().forEach(v->log.debug(v.toString()));
        log.debug("5th:");
        actualValues.subList(10,20).stream().forEach(v->log.debug(v.toString()));
        log.debug("10th:");
        actualValues.subList(20,30).stream().forEach(v->log.debug(v.toString()));
        log.debug("5th again:");
        actualValues.subList(30,40).stream().forEach(v->log.debug(v.toString()));
        log.debug("0th again:");
        actualValues.subList(40,50).stream().forEach(v->log.debug(v.toString()));

        observer.assertValueCount(50);
        observer.assertValues(expectedValues.toArray(new PasteEntry[50]));

    }


    @After
    public void cleanUp(){

        repository.clean().blockingAwait();
        paginator.reinit();

        log.debug("end cleaned up");
    }

    @AfterClass
    public static void cleanUpFinally(){
        final Completable completable = repository.clean().doFinally(() -> {
            if (session != null) {
                session.close();
                session.getCluster().close();
            }
        });
    }

    private Observable<PasteEntry> toObservable(Single<Page<Observable<Row>>> input){
        return input
                .map(page -> page.getContent())
                .toObservable().flatMap(rowObservable -> rowObservable)
                .map(row -> mapToPastEntry().apply(row));
    }

    private Observable<PasteEntry> toObservable(Flowable<Page<Observable<Row>>> input){
        return input
                .map(page -> page.getContent())
                .toObservable().flatMap(rowObservable -> rowObservable)
                .map(row -> mapToPastEntry().apply(row));
    }


    private Function<Row,PasteEntry> mapToPastEntry(){
        return row -> {
            PasteEntry pasteEntry = new PasteEntry();
            pasteEntry.setUuid(row.getUUID("id").toString());
            pasteEntry.setDate(row.getDate("day").toString());
            pasteEntry.setCreatedOnTimestamp(row.getTime("created"));
            pasteEntry.setSecretCode(row.getString("secret"));
            pasteEntry.setTitle(row.getString("title"));
            pasteEntry.setContent(row.getString("content"));
            pasteEntry.setExpiresOnTimestamp(row.getTime("expires"));
            pasteEntry.setPrivate(row.getBool("private"));

            return pasteEntry;
        };
    }

}