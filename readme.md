Here you can see the implementation of REST-like web service that allows clients to share plain-text entries (pastes)

**Used stack**:

* Java 8

* Vertx 3.3.3 (Core,Web)

* VertxNubes 1.3 (Annotation Layer on top of Vertx Web )

* Spring Context 4.3.5 (for DI only)

**Releases**

1.0.0 SNAPSHOT - first implementation

1.1.0 SNAPSHOT - second implementation

* implemented reactive(rxJava2-based) pastes Cassandra repository;

* tests for repo and paginator